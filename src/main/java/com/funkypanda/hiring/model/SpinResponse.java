package com.funkypanda.hiring.model;

public class SpinResponse extends ErrorResponses{

    private int numberResult;

    public SpinResponse(int numberResult) {
        this.numberResult = numberResult;
    }

    public int getNumberResult() {
        return numberResult;
    }

    public void setNumberResult(int numberResult) {
        this.numberResult = numberResult;
    }

}
