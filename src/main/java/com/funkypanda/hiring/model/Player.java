package com.funkypanda.hiring.model;

import javax.persistence.Id;

public class Player {

    private final int id;
    private String gameId;
    private int currency;

    private String typeOfBet;
    private int valueOfBet;

    public Player(){
        id = -1;
    }

    public Player(int id, String gameId) {
        this.id = id;
        this.gameId = gameId;

        this.currency = 100;
        this.typeOfBet = null;
        this.valueOfBet = -1;
    }

    public Player(int id){
        this.id = id;

        this.gameId = null;
        this.currency = 100;
    }

    public int getId() {
        return id;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public String getTypeOfBet() {
        return typeOfBet;
    }

    public void setTypeOfBet(String typeOfBet) {
        this.typeOfBet = typeOfBet;
    }

    public int getValueOfBet() {
        return valueOfBet;
    }

    public void setValueOfBet(int valueOfBet) {
        this.valueOfBet = valueOfBet;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", gameId='" + gameId + '\'' +
                ", currency=" + currency +
                ", typeOfBet='" + typeOfBet + '\'' +
                ", valueOfBet=" + valueOfBet +
                '}';
    }
}
