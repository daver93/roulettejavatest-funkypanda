package com.funkypanda.hiring.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Game extends HashMap {

    private final String gameId;
    private final String creatorId;
    private ArrayList<String> playersInRoom;
    private int currentNumberOfPlayers;
    private boolean open;
    private int lastResult;
    private long lastResultTime;

    private Map<Integer, Integer> bets;

    public Game(String gameId, String creatorId) {
        this.gameId = gameId;
        this.creatorId = creatorId;
        this.open = true;

        this.currentNumberOfPlayers = 1;
        this.playersInRoom = new ArrayList<String>();
        this.playersInRoom.add(creatorId);
        this.lastResult = -1;
        this.lastResultTime = -1;

        bets = new HashMap<Integer, Integer>();
    }

    public String getGameId() {
        return gameId;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public int getCurrentNumberOfPlayers() {
        return currentNumberOfPlayers;
    }

    public void setCurrentNumberOfPlayers(int currentNumberOfPlayers) {
        this.currentNumberOfPlayers = currentNumberOfPlayers;
    }

    public ArrayList<String> getPlayersInRoom() {
        return playersInRoom;
    }

    public void setPlayersInRoom(String addPlayerInRoom) {
        playersInRoom.add(addPlayerInRoom);
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public int getLastResult() {
        return lastResult;
    }

    public void setLastResult(int lastResult) {
        this.lastResult = lastResult;
    }

    public long getLastResultTime() {
        return lastResultTime;
    }

    public void setLastResultTime(long lastResultTime) {
        this.lastResultTime = lastResultTime;
    }

    public Map<Integer, Integer> getBets() {
        return bets;
    }

    public void setBets(Map<Integer, Integer> bets) {
        this.bets = bets;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId='" + gameId + '\'' +
                ", creatorId='" + creatorId + '\'' +
                ", playersInRoom=" + playersInRoom +
                ", currentNumberOfPlayers=" + currentNumberOfPlayers +
                ", open=" + open +
                ", lastResult=" + lastResult +
                ", lastResultTime=" + lastResultTime +
                ", bets=" + bets +
                '}';
    }
}
