package com.funkypanda.hiring.model;

public class JoinGameResponse {

    private boolean succeed;

    public JoinGameResponse() {
    }

    public JoinGameResponse(boolean succeed) {
        this.succeed = succeed;
    }

    public boolean isSucceed() {
        return succeed;
    }

    public void setSucceed(boolean succeed) {
        this.succeed = succeed;
    }
}
