package com.funkypanda.hiring.model;

public class JoinResponse {

    private boolean success;

    public JoinResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
