package com.funkypanda.hiring.model;

import javax.ws.rs.core.Response;

public class ErrorResponses {

    public static enum ErrorStatus {

        NOT_CREATOR(90, "PLAYER IS NOT THE CREATOR OF THIS GAME");

        private final int code;
        private final String reason;

        private ErrorStatus(int statusCode, String reasonPhrase) {
            this.code = statusCode;
            this.reason = reasonPhrase;
        }
    }
}