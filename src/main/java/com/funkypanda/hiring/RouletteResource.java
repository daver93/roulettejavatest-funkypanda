package com.funkypanda.hiring;

import com.funkypanda.hiring.model.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Path("/game")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RouletteResource {

	private final AtomicLong counter;
	private final int MAX_PLAYER_NUMBER = 4;

	private List<Game> games;
	private List<Player> players;

	private final ArrayList<Integer> column1 = new ArrayList<Integer>(Arrays.asList(1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34));
	private final ArrayList<Integer> column2 = new ArrayList<Integer>(Arrays.asList(2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35));
	private final ArrayList<Integer> column3 = new ArrayList<Integer>(Arrays.asList(3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36));

	private final ArrayList<Integer> red = new ArrayList<Integer>(Arrays.asList(1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36));
	private final ArrayList<Integer> black = new ArrayList<Integer>(Arrays.asList(2, 4, 6, 8, 10, 11,	13, 15, 17, 20, 22, 24,	26, 28, 29, 31, 33, 35));

	public RouletteResource(){
		this.counter = new AtomicLong(0);

		games = new ArrayList<Game>();
		players = new ArrayList<Player>();
	}

	public RouletteResource(AtomicLong counter) {
		this.counter = counter;
	}

	@POST
	@Path("/create")
	public CreateGameResponse createGame(@QueryParam("playerId") String player1) {
		try {
			Player pl = findOrCreatePlayer(player1, "", false);

			if (pl != null){
				// Player already exists
				if (pl.getGameId() != null){
					// Player is already in a room
					throw new WebApplicationException(Status.EXPECTATION_FAILED);
				}
				else {

					// Get the next available int as the id for the new room
					String gameId = Long.toString(counter.incrementAndGet());

					// Create the new game room
					Game newGame = new Game(gameId, player1);
					// Keep the new room in a list
					games.add(newGame);

					System.out.println(pl.toString());
					//return the required response
					return new CreateGameResponse(newGame.getGameId());
				}
			}
			else {
				// Get the next available int as the id for the new room
				String gameId = Long.toString(counter.incrementAndGet());

				// Create the new game room
				Game newGame = new Game(gameId, player1);
				// Keep the new room in a list
				games.add(newGame);

				// Create and keep the player in a list
				pl = findOrCreatePlayer(player1, gameId, true);
				if (!players.contains(pl)){
					players.add(pl);
				}


				System.out.println(pl.toString());
				//return the required response
				return new CreateGameResponse(newGame.getGameId());
			}

		} catch (Exception e){
			System.out.println("ERROR AGAIN");
		}

		return new CreateGameResponse();
	}

	@POST
	@Path("/join")
	public JoinGameResponse joinGame(@QueryParam("playerId") String player, @QueryParam("gameId") String gameId){
		try {
			// Find the room
			Game gameJoin = findGame(gameId);
			if (gameJoin != null){
				if (gameJoin.getPlayersInRoom().contains(player)){
					// Player is already in the room
					throw new WebApplicationException("Player is already in the room", Status.EXPECTATION_FAILED);
				}
				else if (gameJoin.getCurrentNumberOfPlayers() >= MAX_PLAYER_NUMBER){
					// Room is full
					throw new WebApplicationException("Room is full", Status.NOT_ACCEPTABLE);
				}
				else if (!gameJoin.isOpen()){
					// Room is closed
					throw new WebApplicationException("Room is closed", Status.NOT_ACCEPTABLE);
				}
				else {
					Player pl = findOrCreatePlayer(player, gameId, false);
					if (pl != null){
						if (pl.getGameId() != null){
							throw new WebApplicationException("Player is already in a room", Status.EXPECTATION_FAILED);
						}
						else {
							// Put the player in the room
							gameJoin.setPlayersInRoom(player);
							// Increase the number of player in the room
							gameJoin.setCurrentNumberOfPlayers(gameJoin.getPlayersInRoom().size());

							return new JoinGameResponse(true);
						}
					}
					else {
						pl = findOrCreatePlayer(player, gameId, true);
						if (!players.contains(pl)){
							// Keep the player in a list
							players.add(pl);
						}
						// Put the player in the room
						gameJoin.setPlayersInRoom(player);
						// Increase the number of player in the room
						gameJoin.setCurrentNumberOfPlayers(gameJoin.getPlayersInRoom().size());

						return new JoinGameResponse(true);
					}
				}
			}
			else {
				throw new WebApplicationException("Game does not exists", Status.NOT_FOUND);
			}
		} catch (Exception e){
			e.printStackTrace();
		}

		// Return fail message
		return new JoinGameResponse(false);
	}

	@POST
	@Path("/leave")
	public LeaveGameResponse leaveGame(@QueryParam("playerId") String player, @QueryParam("gameId") String gameId){
		try {
			// Find the room
			Game gameLeave = findGame(gameId);
			if (gameLeave != null){
				if ( !gameLeave.getPlayersInRoom().contains(player) ){
					throw new WebApplicationException("Player is already in another room", Status.EXPECTATION_FAILED);
				}
				else {
					// Remove the player
					gameLeave.getPlayersInRoom().remove(player);

					Player pl = findOrCreatePlayer(player, gameId, false);

					if (pl != null) {
						// Player already exists
						pl.setGameId(null);
					}
					else {
						throw new WebApplicationException("Player does not exist", Status.NOT_FOUND);
					}

					// Update the number of players
					gameLeave.setCurrentNumberOfPlayers(gameLeave.getPlayersInRoom().size());

					if (gameLeave.getCreatorId().equals(player)){
						// Close the room (the creator left it)
						gameLeave.setOpen(false);
						// Remove all the players from the room
						for (String playerId : gameLeave.getPlayersInRoom()){

							pl = findOrCreatePlayer(playerId, gameId, false);
							if (pl != null) {
								// Player already exists
								pl.setGameId(null);
							}
							else {
								//throw new WebApplicationException("Player does not exist", Status.NOT_FOUND);
							}
						}
						// Clear the list of players from the closed room
						gameLeave.getPlayersInRoom().clear();
						// Update the number of players
						gameLeave.setCurrentNumberOfPlayers(gameLeave.getPlayersInRoom().size());
					}
				}
			}
			else {
				throw new WebApplicationException("Game does not exists", Status.NOT_FOUND);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}

	@PUT
	@Path("/{gameId}")
	public Bet makeBet(@QueryParam("playerId") String player, @QueryParam("betAmount") int betAmount, @QueryParam("betType") String betType, @QueryParam("betTypeValue") int betTypeValue, @Context HttpServletRequest request){
		try {
			// Get the game id passed from url
			String url = request.getRequestURL().toString();
			int index=url.lastIndexOf('/');
			String gameId = url.substring(index+1, url.length());

			Game game = findGame(gameId);
			if (game != null){
				if ( !game.getPlayersInRoom().contains(player) ){
					throw new WebApplicationException("Player is already in another room", Status.EXPECTATION_FAILED);
				}
				else {
					// Find the player
					Player thePlayer = findOrCreatePlayer(player, gameId, false);
					if (thePlayer == null){
						throw new WebApplicationException("Player does not exist", Status.NOT_FOUND);
					}
					else {
						// Check if the attempt of bet is valid
						if(validateBet(betAmount, betType, betTypeValue, thePlayer)){
							// Remove the bet amount from the total player's currency
							thePlayer.setCurrency(thePlayer.getCurrency()-betAmount);
							game.getBets().put(thePlayer.getId(), betAmount);

							// return the bet
							return new Bet(player, betAmount, betType, betTypeValue);
						}
					}
				}
			}
			else {
				throw new WebApplicationException("Game does not exists", Status.NOT_FOUND);
			}
		} catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}

	@GET
	@Path("/{gameId}")
	public GameState getGame(@PathParam("gameId") String gameId) {
		try {
			Game gm = findGame(gameId);
			if (gm != null){
				return new GameState(gm.isOpen(), gm.getLastResult(), gm.getCurrentNumberOfPlayers(), gm.getLastResultTime());
			}
			else {
				throw new WebApplicationException("Game does not exist", Status.NOT_FOUND);
			}

		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@PUT
	@Path("/spin/{gameId}")
	public SpinResponse spin(@QueryParam("playerId") String player, @Context HttpServletRequest request){

		try{
			// Get the game id passed from url
			String url = request.getRequestURL().toString();
			int index=url.lastIndexOf('/');
			String gameId = url.substring(index+1, url.length());

			Game game = findGame(gameId);
			if (game != null){
				System.out.println(game.toString());

				if (game.getCreatorId().equals(player)){
					if (game.getPlayersInRoom().contains(player)){
						// Get a random number between 0 and 36
						int spinResult = new Random().ints(0, 37).findFirst().getAsInt();
						game.setLastResult(spinResult);
						game.setLastResultTime(Instant.now().getEpochSecond());

						payoutHandler(spinResult, game);

						// return response result
						return new SpinResponse(spinResult);
					}
					else {
						// This player is NOT the creator, so an error must be appeared
						throw new WebApplicationException("Player is not in the room", Status.UNAUTHORIZED);
					}
				}
				else {
					// This player is NOT the creator, so an error must be appeared
					throw new WebApplicationException("Player is not the creator [unauthorized to spin it]", Status.UNAUTHORIZED);
				}
			}
			else {
				throw new WebApplicationException("Game does not exists", Status.NOT_FOUND);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	// Method to reset bet for each player after the payout process
	private void resetBeting(Player player){
		// Reset bets
		player.setTypeOfBet(null);
		player.setValueOfBet(-1);
	}

	// Types of available bets
	private enum BetTypes{
		number,
		column,
		row,
		oddoreven,
		dozen,
		range19,
		topLine,
		redorblack
	}

	// Method to get the Game object for a specific gameId
	private Game findGame(String gameId){
		for (Game game : games){
			if (game.getGameId().equals(gameId)){
				// Get the game
				return game;
			}
		}
		return null;
	}

	// Method to get the player (if exists), or create him if he does not exist and createIfNeed = true
	private Player findOrCreatePlayer(String player, String gameId, boolean createIfNeed){
		try {
			// Keep the playerId as an Integer
			int playerId = Integer.parseInt(player);

			for (Player p : players){
				if (p.getId() == playerId){
					// Get the player
					return p;
				}
			}

			if (createIfNeed){
				// Create the player
				return new Player(playerId, gameId);
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	// Method to validate that the bet is correct
	private boolean validateBet(int betAmount, String betType, int betTypeValue, Player player){
		boolean isValid = false;
		// Check if player has enough currency for this bet
		if (betAmount <= player.getCurrency() && player.getCurrency() > 0){
			// Check if betType is correct
			if (betType.equals(BetTypes.redorblack.toString())){
				if (betTypeValue < 0 || betTypeValue > 1){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}
			else if (betType.equals(BetTypes.oddoreven.toString())){
				if (betTypeValue < 0 || betTypeValue > 1){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}
			else if (betType.equals(BetTypes.number.toString())){
				if (betTypeValue < 0 || betTypeValue > 36){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}
			else if (betType.equals(BetTypes.dozen.toString())){
				if (betTypeValue <= 0 || betTypeValue > 3){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}
			else if (betType.equals(BetTypes.column.toString())){
				if (betTypeValue <= 0 || betTypeValue > 3){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}
			else if (betType.equals(BetTypes.row.toString())){
				if (betTypeValue != 0){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}
			else if (betType.equals(BetTypes.topLine.toString())){
				if (betTypeValue != 1){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}
			else if (betType.equals(BetTypes.range19.toString())){
				if (betTypeValue < 1 || betTypeValue > 2){
					throw new WebApplicationException("Wrong Bet Type Value", Status.BAD_REQUEST);
				}
				isValid = true;
			}

			if (isValid){
				player.setTypeOfBet(betType);
				player.setValueOfBet(betTypeValue);
			}
		}
		else {
			System.out.println("PLAYER DOES NOT HAVE THIS AMOUNT OF CURRENCY");
			return false;
		}
		return isValid;
	}

	// A method to handle payout after the successful spin action
	private void payoutHandler(int result, Game game){
		try{
			for (String player : game.getPlayersInRoom()){

				System.out.println("Player " + player + " betted " + game.getBets().get(Integer.parseInt(player)));

				// Find the player
				Player thePlayer = findOrCreatePlayer(player, game.getGameId(), false);
				if (thePlayer != null){
					if (thePlayer.getTypeOfBet() != null){
						System.out.println("Player has id " + thePlayer.getId() + " and bet on " + thePlayer.getTypeOfBet() + " with value of " + thePlayer.getValueOfBet());

						if (thePlayer.getTypeOfBet().equals(BetTypes.number.toString())){
							if (thePlayer.getValueOfBet() == result){
								System.out.println("Player Won");
								// wins 35 to 1
								thePlayer.setCurrency(thePlayer.getCurrency() + 35 + game.getBets().get(Integer.parseInt(player)));
							}
							else {
								System.out.println("Player Lost");
							}
							// Reset bets
							resetBeting(thePlayer);
						}
						// Check for odd or even
						else if (thePlayer.getTypeOfBet().equals(BetTypes.oddoreven.toString())){
							if (thePlayer.getValueOfBet() == 0){ //even
								if (result % 2 == 1){
									// Result is an odd number
									System.out.println("Player won");
									// wins 1 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 1 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							else if (thePlayer.getValueOfBet() == 1){ //odd
								if (result % 2 == 0){
									// Result is an even number
									System.out.println("Player won");
									// wins 1 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 1 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							// Reset bets
							resetBeting(thePlayer);
						}
						// Check for red or black
						else if (thePlayer.getTypeOfBet().equals(BetTypes.redorblack.toString())){
							if (thePlayer.getValueOfBet() == 0){ // red
								if (red.contains(result)){
									// Result is an odd number
									System.out.println("Player won");
									// wins 1 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 1 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							else if (thePlayer.getValueOfBet() == 1){ // black
								if (black.contains(result)){
									// Result is an even number
									System.out.println("Player won");
									// wins 1 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 1 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							// Reset bets
							resetBeting(thePlayer);
						}
						// Check for column
						else if (thePlayer.getTypeOfBet().equals(BetTypes.column.toString())){
							if (thePlayer.getValueOfBet() == 1){
								// Bet is on column 1
								if (column1.contains(result)){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 2 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							else if (thePlayer.getValueOfBet() == 2){
								// Bet is on column 2
								if (column2.contains(result)){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 2 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							else if (thePlayer.getValueOfBet() == 3){
								// Bet is on column 3
								if (column3.contains(result)){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 2 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							// Reset bets
							resetBeting(thePlayer);
						}
						// Check for dozen
						else if (thePlayer.getTypeOfBet().equals(BetTypes.dozen.toString())){
							if (thePlayer.getValueOfBet() == 1){
								// Bet is on column 1
								if (result > 0 & result <= 12){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 2 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							else if (thePlayer.getValueOfBet() == 2){
								// Bet is on column 2
								if (result > 12 & result <= 24){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 2 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							else if (thePlayer.getValueOfBet() == 3){
								// Bet is on column 3
								if (result > 24 & result <= 36){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 2 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							// Reset bets
							resetBeting(thePlayer);
						}
						// Check for range: [1 to 18] or [19 to 36]
						else if (thePlayer.getTypeOfBet().equals(BetTypes.range19.toString())){
							if (thePlayer.getValueOfBet() == 1){
								// Bet is on column 3
								if (result > 0 & result <= 18){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 1 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							else if (thePlayer.getValueOfBet() == 2){
								// Bet is on column 3
								if (result > 18 & result <= 36){
									System.out.println("Player won");
									// wins 2 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 1 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							// Reset bets
							resetBeting(thePlayer);
						}
						// Check for row (0)
						else if (thePlayer.getTypeOfBet().equals(BetTypes.row.toString())){
							if (thePlayer.getValueOfBet() == 0){
								if (result == 0){
									System.out.println("Player won");
									// wins 35 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 35 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							// Reset bets
							resetBeting(thePlayer);
						}
						// Check for top line
						else if (thePlayer.getTypeOfBet().equals(BetTypes.topLine.toString())){
							if (thePlayer.getValueOfBet() == 1){
								if (result >= 0 && result <= 3){
									System.out.println("Player won");
									// wins 8 to 1
									thePlayer.setCurrency(thePlayer.getCurrency() + 8 + game.getBets().get(Integer.parseInt(player)));
								}
							}
							// Reset bets
							resetBeting(thePlayer);
						}
					}
				}
				System.out.println(thePlayer.toString());
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}